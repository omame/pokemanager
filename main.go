package main

import (
	"fmt"
	"os"

	"github.com/sirupsen/logrus"

	"gitlab.com/yakshaving.art/slack/pokemanager/internal/config"
	"gitlab.com/yakshaving.art/slack/pokemanager/internal/pokemanager"
	"gitlab.com/yakshaving.art/slack/pokemanager/internal/slack"
	"gitlab.com/yakshaving.art/slack/pokemanager/internal/version"
)

func main() {
	args := config.ParseArgs()

	if args.Version {
		fmt.Println(version.GetVersion())
		os.Exit(0)
	}

	if args.SlackToken == "" {
		logrus.Fatal("Slack token not set")
	}

	formatter := &logrus.TextFormatter{
		FullTimestamp:   true,
		TimestampFormat: "2006-01-02 15:04:05",
	}
	logrus.SetFormatter(formatter)
	logrus.SetLevel(logrus.InfoLevel)
	if args.Debug {
		logrus.SetLevel(logrus.DebugLevel)
	}

	conf, err := config.Load(args.ConfigFile)
	if err != nil {
		logrus.Fatal(err)
	}

	client := slack.Connect(args.SlackToken, args.Debug)
	go client.Listen()
	logrus.Infoln("pokemanager is starting")
	pokemanager.Run(conf, client)
}
