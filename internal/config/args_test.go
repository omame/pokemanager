package config_test

import (
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/yakshaving.art/slack/pokemanager/internal/config"
)

func TestParseArgs(t *testing.T) {
	a := assert.New(t)
	tc := config.Args{
		ConfigFile: "pokemanager.yml",
		Debug:      false,
		SlackToken: "xxx-xx-xx-xxxx",
		Version:    false,
	}
	os.Setenv("SLACK_TOKEN", "xxx-xx-xx-xxxx")
	args := config.ParseArgs()
	t.Run("default arguments", func(t *testing.T) {
		a.Equal(args, tc)
	})
}
